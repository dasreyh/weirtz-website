var colors = [
    "rgb(255, 0, 0)",
    "rgb(255, 255, 0)",
    "rgb(255, 50, 0)",
    "rgb(20, 0, 255)",
    "rgb(150, 70, 9)",
    "rgb(255, 95, 0)"
]

var squares = document.querySelectorAll(".square");
var pickedColor = pickedColor();
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector("#message");

colorDisplay.textContent = pickedColor;

for(var i = 0; i < squares.length; i++){
    //Add initial colors to squares.
    squares[i].style.backgroundColor = colors[i];
    //Add click listeners to squares.
    squares[i].addEventListener("click",function(){
        //Grab color of clicked square
        var clickedColor = this.style.backgroundColor;
        //compare
        if(clickedColor === pickedColor){
            messageDisplay.textContent = "Correct!"
            changeColors(clickedColor);
        }else{
            this.style.backgroundColor = "#232323";
            messageDisplay.textContent = "Try Again, Jerk"
        }
    });
}

function changeColors(color){
    //loop through all squares and change each color to the given color
    for (var i = 0; i < squares.length; i++){
        squares[i].style.backgroundColor = color;
    }
}

function pickColor(){
    Math.floor(Math.random() * 6 + 1);
}