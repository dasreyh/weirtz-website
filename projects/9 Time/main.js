$.getJSON('https://api.unsplash.com/photos/random/?client_id=e61354ba453d280ecf6d0f4178ca4264709f25e5d6f21b085ff97b8854d88703', function(data) {
  console.log(data);
  
  $.each(data, function(index, value) {
    console.log(value);
    

    var imageURL = value.urls.regular;
    
    // $('.name').text(name);
    // $('.bio').text(bio);
    // $('.image img').attr('src', imageURL);
    
    $('.output').append('<div class="image"><img src="' + imageURL + '"/></div>');
  });
});





//GET TIME
var getTimeObj = document.querySelector("#time");
var getTimeObjSec = document.querySelector("#seconds");
var getTimeObjMill = document.querySelector("#milliseconds");



setInterval(function(){
    getTime();
}, 1);

function getTime(){
    var date = new Date();
    var time = date.getHours() + ":" +  date.getMinutes();
    var timeSec = date.getSeconds();
    var timeMill = date.getMilliseconds();
    getTimeObj.textContent = time;
    getTimeObjSec.textContent = timeSec;
    getTimeObjMill.textContent = timeMill;

}