var p1Button = document.querySelector("#p1");
var p2Button = document.querySelector("#p2");
var p1Score = 0;
var p2Score = 0;
var score1 = document.querySelector("#score1");
var score2 = document.querySelector("#score2");
var gameOver = false;
var resetBut = document.querySelector("#reset");
var winningScore = 5;
var numInput = document.querySelector("input[type='number']");
var playingTo = document.querySelector("#playingTo");

p1Button.addEventListener("click", function(){
    if(!gameOver){
        p1Score++;
        score1.textContent = p1Score;
        if(p1Score === winningScore){
            gameOver = true;
            score1.classList.add("winner");
        }else{
            
        }
    }
});

p2Button.addEventListener("click", function(){
    if(!gameOver){
        p2Score++;
        score2.textContent = p2Score;
        if(p2Score === winningScore){
            gameOver = true;
            score2.classList.add("winner");
        }else{
            
            
        }
    }
});

function reset(){
    p1Score = 0;
    p2Score = 0;
    score1.textContent = p1Score;
    score2.textContent = p2Score;
    gameOver = false;
    score1.classList.remove("winner");
    score2.classList.remove("winner");
};

resetBut.addEventListener("click", function(){
    reset();
});

numInput.addEventListener("change", function(){
    playingTo.textContent = numInput.value;
    winningScore = Number(numInput.value);
    reset();
});